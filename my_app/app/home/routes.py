# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

import simplejson as json

from app import tryton
from app.home import blueprint
from flask import render_template, redirect, url_for, request, session

from functools import wraps
from jinja2 import TemplateNotFound

from babel.dates import format_date
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from sql import Null
from sql.aggregate import Sum, Count
from sql.conditionals import Coalesce, Case
from sql.functions import Round

from trytond.transaction import Transaction

Session = tryton.pool.get('web.user.session')
Appointments = tryton.pool.get('gnuhealth.appointment')
ContactTracing = tryton.pool.get('gnuhealth.contact_tracing')
ContactTracingCall = tryton.pool.get('gnuhealth.contact_tracing_call')
Patient = tryton.pool.get('gnuhealth.patient')

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        session_key = None
        if 'session_key' in session:
            session_key = session['session_key']
        user = Session.get_user(session_key)
        if not user:
            return redirect(url_for('login', next=request.path))
        return func(*args, **kwargs)
    return wrapper

@tryton.transaction()
def population_counting():
    population = {}
    week_ago = datetime.now()-timedelta(days=7)
    patients = Patient.search([('id','>',0)])
    population['this_week_new_patients'] = len([x for x in patients if x.create_date > week_ago and x.active])
    population['total_population'] = len([x for x in patients if x.active])
    population['range'] = ['0','1 - 4']
    #population < 1 year
    population['masculine'] = [len([x for x in patients if x.age_float<1 and x.gender == 'm'])]
    population['femenine'] = [len([x for x in patients if x.age_float<1 and x.gender == 'f'])]
    population['total'] = [population['masculine'][-1]+population['femenine'][-1]]
    #population <4, >1
    population['masculine'].append(len([x for x in patients if x.age_float<4 and  x.age_float>1 and x.gender == 'm']))
    population['femenine'].append(len([x for x in patients if x.age_float<4 and  x.age_float>1 and x.gender == 'f']))
    population['total'].append(population['masculine'][-1]+population['femenine'][-1])
    for lower,upper in zip(range(5,80,5),range(9,80,5)):
        population['range'].append(str(lower)+' - ' + str(upper))
        population['masculine'].append(len([x for x in patients if x.age_float<upper and  x.age_float>lower and x.gender == 'm']))
        population['femenine'].append(len([x for x in patients if x.age_float<upper and  x.age_float>lower and x.gender == 'f']))
        population['total'].append(population['masculine'][-1]+population['femenine'][-1])
    #population > 80 years
    population['range'].append('80 y +')
    population['masculine'].append(len([x for x in patients if x.age_float>80 and x.gender == 'm']))
    population['femenine'].append(len([x for x in patients if x.age_float>80 and x.gender == 'f']))
    #reverse the list for the piramid
    population['range'].reverse()
    population['masculine'].reverse()
    population['femenine'].reverse()
    population['total'].reverse()
    for m,f in zip(population['femenine'], population['masculine']):
        population['total'].append(m+f)
    return population

@tryton.transaction()
def last_week_appointments():
    res = {}
    days_before = range(7)
    dows_name = {'0':'lunes', '1':'martes', '2':'miércoles', '3':'jueves',
                 '4':'viernes' ,'5':'sábado', '6':'domingo'}
    res['total_confirmed'] = 0
    res['total_done'] = 0
    for x in days_before[::-1]:
        dow_date = datetime.today() - timedelta(days=x)
        dow_appointments = Appointments.search([
                                    ('appointment_date','>',dow_date.replace(hour=0,minute=0,second=0)),
                                    ('appointment_date','<',dow_date.replace(hour=23,minute=59,second=59))
                                    ])
        res['day_'+str(x)] = {
                        'total_appointments': len(dow_appointments),
                        'weekday': dows_name[str(dow_date.weekday())]
                        }
        res['total_confirmed'] += len(dow_appointments)
        res['total_done'] += len([x for x in dow_appointments if x.state in ['done','checked_in']])
    return res

@tryton.transaction()
def contact_tracing_stats():
    res =  {}
    contact_tracing = ContactTracing.search([('id','>',1700)])
    contact_tracing = sorted(contact_tracing, key=lambda ct: ct.first_contact)
    contact_tracing_call = ContactTracingCall.search([('name','in',[x.id for x in contact_tracing])])
    res["total_recuperated"] = len([x for x in contact_tracing if x.result == "r"])
    res["total_deceased"] = len([x for x in contact_tracing if x.result == "d"])
    res["daily_count"] = []
    #by transaction
    transaction = Transaction()
    connection = transaction.connection
    cursor = connection.cursor()

    ct_table = ContactTracing.__table__()
    ctCall_table = ContactTracingCall.__table__()
    #query0 = ct_table.select(Count(ct_table.id),ct_table.first_contact,
            #group_by=first_contact,
            #order_by=ct_table.first_contact)
    #cursor.execute(*query0)
    #TODO
                #"day": day_str,
                #"count": count,
                #"accumulate": accumulate,
                #"calls": calls,
                #"accumulate_calls": accumulate_calls,
                #"positives": positives,
                #"accumulate_positives": accumulate_positives,
                #"negatives": negatives,
                #"accumulate_negatives": accumulate_negatives,
    
    
    query0 = 'SELECT COUNT(*) as count, SUM(COUNT(*)) OVER (ORDER BY first_contact::date)::INTEGER as acc_first_contact, '\
                +'first_contact::date '\
                +'FROM ' + ContactTracing._table+' '\
                +'GROUP BY first_contact::date'
    print('1'*20+'\n',query0)
    cursor.execute(query0)
    result_first_contact = cursor.fetchall()
    print('*'*20+'\n',result_first_contact)

    query1 = 'SELECT COUNT(*) as count, SUM(COUNT(*)) OVER (ORDER BY date::date)::INTEGER as acc_call_date, '\
                +'date::date '\
                +'FROM ' + ContactTracingCall._table+' '\
                +'GROUP BY date::date'
    cursor.execute(query1)
    result_call_date = cursor.fetchall()
    print('0'*20+'\n', result_call_date)

    if contact_tracing:
        date_beginning = contact_tracing[0].first_contact
        date_beginning = min([x.first_contact for x in contact_tracing])
        date_end = max([x.first_contact for x in contact_tracing])
        total_days = (date_end - date_beginning).days+2
        accumulate = 0
        accumulate_calls = 0
        accumulate_positives = 0
        accumulate_negatives = 0
        #fullfilling daily_counting
        for x in range(total_days):
            day = date_beginning + timedelta(days=x)
            day_str = format_date(day.date(), locale='es')
            count = len([x for x in contact_tracing if x.first_contact and x.first_contact.date() == day.date()])
            calls = len([x for x in contact_tracing_call if x.date and x.date.date() == day.date()])
            positives = len([x for x in contact_tracing\
                    if (((x.swabbing_date == day.date()) !=  (x.first_contact.date() == day.date()))\
                        or (x.swabbing_date == x.first_contact.date() == day.date()))
                        and x.swabbing_result == 'positive'])
            negatives = len([x for x in contact_tracing\
                    if (((x.swabbing_date == day.date()) !=  (x.first_contact.date() == day.date()))\
                        or (x.swabbing_date == x.first_contact.date() == day.date()))
                        and x.swabbing_result == 'negative'])
            accumulate += count
            accumulate_calls += calls
            accumulate_positives += positives
            accumulate_negatives += negatives
            res["daily_count"].append({
                "day": day_str,
                "count": count,
                "accumulate": accumulate,
                "calls": calls,
                "accumulate_calls": accumulate_calls,
                "positives": positives,
                "accumulate_positives": accumulate_positives,
                "negatives": negatives,
                "accumulate_negatives": accumulate_negatives,
                })
    else:
        res["date_beginning"] = 'sin casos registrados aún'
    #getting the max value for daily count
    res['max_daily'] = max(max(max(
                            [x['count'] for x in res['daily_count']],
                            [x['calls'] for x in res['daily_count']]),
                            [x['positives'] for x in res['daily_count']]
                                    ))
    res['max_accumulative'] = max(
                            res['daily_count'][-1]['accumulate'],
                            res['daily_count'][-1]['accumulate_calls'],
                            res['daily_count'][-1]['accumulate_positives']
                            )
    return res


@login_required
@tryton.transaction()
@blueprint.route('/index')
def index():
    if 'identified' in session and session['identified']:
        return render_template('index.html', segment='index',lw_appointments=last_week_appointments())
    return redirect(url_for('base_blueprint.login'))

@login_required
@tryton.transaction()
@blueprint.route('/covid_dashboard')
def covid_dashboard():
    return render_template('covid_dashboard.html',
        lw_appointments=last_week_appointments(),
        ct=contact_tracing_stats(),
        ct_json=json.dumps(contact_tracing_stats()) )

@login_required
@tryton.transaction()
@blueprint.route('/patient_population')
def patient_population():
    #ct = contact_tracing_stats()
    return render_template('patient-population.html',
        population=population_counting(),
        )

@login_required
@tryton.transaction()
@blueprint.route('/<template>')
def route_template(template):
    try:
        if not template.endswith( '.html' ):
            template += '.html'
        # Detect the current page
        segment = get_segment( request )
        # Serve the file (if exists) from app/templates/FILE.html
        return render_template(template,segment=segment)
    except TemplateNotFound:
        return render_template('page-404.html'), 404
    except:
        return render_template('page-500.html'), 500

# Helper - Extract current page name from request 
def get_segment( request ): 
    try:
        segment = request.path.split('/')[-1]
        if segment == '':
            segment = 'index'
        return segment
    except:
        return None
