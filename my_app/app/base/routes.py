from flask import render_template, redirect, request, url_for, flash, session

from app import tryton
from app.base import blueprint
from app.base.forms import LoginForm
import app.base.models 

from functools import wraps

WebUser = tryton.pool.get('web.user')
Session = tryton.pool.get('web.user.session')

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        session_key = None
        if 'session_key' in session:
            session_key = session['session_key']
        user = Session.get_user(session_key)
        if not user:
            return redirect(url_for('login', next=request.path))
        return func(*args, **kwargs)
    return wrapper

@blueprint.route('/')
def route_default():
    return redirect(url_for('base_blueprint.login'))

## Login

@blueprint.route('/login', methods=['GET', 'POST'])
@tryton.transaction()
def login():
    login_form = LoginForm(request.form)
    if 'login' in request.form:
        #request.method == 'POST' and login_form.validate_on_submit():
        try:
            webuser = WebUser.authenticate(login_form.email.data, login_form.password.data)
            session['identified'] = False
            if webuser:
                session['session_key'] = WebUser.new_session(webuser)
                session['identified'] = True
                return redirect(url_for('base_blueprint.route_default'))
            flash('Verifique sus credenciales', 'error')
            return render_template( 'accounts/login.html', msg='Correo electrónico o contraseña incorrecta', form=login_form)
        except:
            return 'Demasiados intentos de ingreso o algun otro error. Espere un momento y vuelvalo a intentar'
    return render_template('accounts/login.html', form=login_form)

# Logout
@login_required
@blueprint.route('/logout')
@tryton.transaction(readonly=False)
def logout():
    if session['session_key']:
        Session.delete(Session.search(
                ('key', '=', session['session_key']),
                ))
        session.pop('session_key', None)
        session.pop('identified', None)
        flash("Se ha desconectado tu sesión", 'success')
    return redirect(url_for('base_blueprint.login'))



## Errors

@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template('page-403.html'), 403

@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template('page-404.html'), 404

@blueprint.errorhandler(500)
def internal_error(error):
    return render_template('page-500.html'), 500
